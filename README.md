# README.md file for twostep repository
This is a respository of R analyses of data from Daw's Two Step task (Daw et al., 2011). 
Partcipants completed 6 runs (n trials) of the Daw Two-Step task while undergoing fMRI.
Prior to scanning, participants also completed 4 blocks of the task for practice.