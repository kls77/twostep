// this is the data that get passed into the program

data {
	int NS; // number of subjects
	int MT; // max number of trials per subject

	int NT[NS]; // number of actual trials for each subject

	int r[NS,MT];  // 1 or -1
	int c1[NS,MT]; // 0 or 1
	int c2[NS,MT]; // 0 or 1
	int st[NS,MT]; // 1 or 2

	// this is between-subject covariates

	real age[NS];
}

parameters {

	vector[7] betams; // group level means for the 7 model parameters

	// variance/covariance on per subject parameters around that mean
	// separates into scale vector and
	// correlation mtx, see example on p65 of stan documentation

	vector<lower=0>[7] tau;	// covariance scale
	corr_matrix[7] omega;   // covariance correlation

	vector[7] betaage;         // regression weights by which age affects params

	vector[7] betas[NS];			// 7 x NS per subejct parameters
}

// change of variables to get covariance matrix from tau and omega

transformed parameters {
	matrix[7,7] sigma;

	sigma = quad_form_diag(omega,tau);
}


model {
	// priors for the various parameters

	omega ~ lkj_corr(2);
	tau ~ normal(0,1);

	betams ~ normal(0,1);

	betaage ~ normal(0,1);

	// per subject model

	for (s in 1:NS) {

		int pc;
		int pc2[2];
		int tcounts[2,2];
		real qm[2];
		real qt10[2];
		real qt11[2];
		real qt2[2,2];

		real beta1m;
		real beta1t0;
		real beta1t1;
		real betac;
		real beta2;
		real beta2c;
		real alpha1;

		// draw subject-level parameter vector

		betas[s] ~ multi_normal(betams + betaage * age[s], sigma);

		// now give names to the 7 individual parameters in it

		beta1m =betas[s][1];
		beta1t0 = betas[s][2];
		beta1t1 = betas[s][3];
		betac = betas[s][4];
		beta2 = betas[s][5];
		beta2c = betas[s][6];

		// Phi_approx transforms the unbounded parameters to a learning rate in 0/1
		// 2.2 is sqrt(5), which rescales in attempt to make the prior uniform in 0/1 a priori

		alpha1 = Phi_approx(betas[s][7]/2.2);
		for (i in 1:2) for (j in 1:2) tcounts[i,j] = 0;
		for (i in 1:2) {
			qm[i] = 0;
			qt10[i] = 0;
			qt11[i] = 0;
		}
		for (i in 1:2) for (j in 1:2) {
			qt2[i,j] = 0;
		}


		pc = 0;
		pc2[1] = 0;
		pc2[2] = 0;

		for (t in 1:NT[s]) {
			int nc1;
			int nc2;
			int ns;

			if(c1[s,t] >= 0) {

				// this is picking out the model based Q values according to the transition
				// matrix (just decides which action goes with which state)

				qm[1] = int_step(tcounts[1,1]+tcounts[2,2]-tcounts[1,2]-tcounts[2,1]) ?
				fmax(qt2[1,1],qt2[1,2]) : fmax(qt2[2,1],qt2[2,2]);

				qm[2] = int_step(tcounts[1,1]+tcounts[2,2]-tcounts[1,2]-tcounts[2,1]) ?
				fmax(qt2[2,1],qt2[2,2]) : fmax(qt2[1,1],qt2[1,2]);

				// first level choice

				c1[s,t] ~ bernoulli_logit(beta1m  * (qm[2] - qm[1])
					+ beta1t0  * (qt10[2] - qt10[1])
					+ beta1t1  * (qt11[2] - qt11[1])
					+ betac * pc );

				// update transition matrix

				tcounts[c1[s,t]+1,st[s,t]] = tcounts[c1[s,t]+1,st[s,t]] + 1;

				if(c2[s,t] >= 0) {

					// second level choice

					c2[s,t] ~ bernoulli_logit(beta2  * (qt2[st[s,t],2] - qt2[st[s,t],1]) + beta2c * pc2[st[s,t]]);

					// outcomes in all learning steps divided through by alpha here to decorrelate alpha & beta, as in EWA

					qt10[c1[s,t]+1] = qt10[c1[s,t]+1] * (1 - alpha1) + qt2[st[s,t],c2[s,t]+1];
					qt11[c1[s,t]+1] = qt11[c1[s,t]+1] * (1 - alpha1) + r[s,t];
					qt2[st[s,t],c2[s,t]+1] = qt2[st[s,t],c2[s,t]+1] * (1 - alpha1) + r[s,t];

					// decay unchosen options

					nc1 = 2 - c1[s,t];
					nc2 = 2 - c2[s,t];
					ns = 3 - st[s,t];

					qt10[nc1] = qt10[nc1] * (1 - alpha1);
					qt11[nc1] = qt11[nc1] * (1 - alpha1);
					qt2[st[s,t], nc2] = qt2[st[s,t],nc2] * (1 - alpha1);
					qt2[ns,1] = qt2[ns,1] * (1 - alpha1);
					qt2[ns,2] = qt2[ns,2] * (1 - alpha1);

					pc2[st[s,t]] = 2* c2[s,t] - 1;

				} // if c2 exists

			pc = 2 * c1[s,t] - 1;

			}  // if c1 exists

		}

	}
}
